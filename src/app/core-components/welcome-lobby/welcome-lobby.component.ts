import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-welcome-lobby',
  templateUrl: './welcome-lobby.component.html',
  styleUrls: ['./welcome-lobby.component.scss']
})
export class WelcomeLobbyComponent implements OnInit {
  showPoster=false;
  liveMsg = false;
  socketMsg;
  constructor(private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-135');
    this.chat.getMessages().subscribe((data => {
      console.log('data',data);
     let check = data.split('_');
     if(check[0]=="start" && check[1]=="live"){
       this.liveMsg = true;
       console.log(check[2]);
       this.socketMsg = check[2];
     }
     if(check[0] == 'stop' && check[1]== 'live'){
      this.liveMsg = false;
     }

    }));
  }
  endPlayscreen(){
    this.showPoster = true;
  }
  playScreenOnImg(){
    this.showPoster = false;
    let playVideo: any = document.getElementById("playVideo");
    playVideo.play();  
  }
  openpopup(){
    if (localStorage.getItem('whatever')) {
      $('.NoFeedback').modal('show');
    }else{
      $('.feebackModal').modal('show');
    }
  }
  closePopup() {
    $('.NoFeedback').modal('hide');
}
}