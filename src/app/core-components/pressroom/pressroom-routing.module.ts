import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PressroomComponent } from './pressroom.component';


const routes: Routes = [
  {path:'', component:PressroomComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PressroomRoutingModule { }
