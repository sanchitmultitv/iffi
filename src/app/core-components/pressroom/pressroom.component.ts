import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
@Component({
  selector: 'app-pressroom',
  templateUrl: './pressroom.component.html',
  styleUrls: ['./pressroom.component.scss']
})
export class PressroomComponent implements OnInit {
  liveMsg = false;
  socketMsg;
  constructor(private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-135');
    this.chat.getMessages().subscribe((data => {
      console.log('data',data);
     let check = data.split('_');
     if(check[0]=="start" && check[1]=="live"){
       this.liveMsg = true;
       console.log(check[2]);
       this.socketMsg = check[2];
     }
     if(check[0] == 'stop' && check[1]== 'live'){
      this.liveMsg = false;
     }
    }));
  }

}
