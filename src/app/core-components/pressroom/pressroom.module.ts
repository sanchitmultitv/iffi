import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PressroomRoutingModule } from './pressroom-routing.module';
import { PressroomComponent } from './pressroom.component';


@NgModule({
  declarations: [PressroomComponent],
  imports: [
    CommonModule,
    PressroomRoutingModule
  ]
})
export class PressroomModule { }
