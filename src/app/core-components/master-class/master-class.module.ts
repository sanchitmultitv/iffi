import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterClassRoutingModule } from './master-class-routing.module';
import { Masterclass1Component } from './components/masterclass1/masterclass1.component';
import { Masterclass2Component } from './components/masterclass2/masterclass2.component';
import { Masterclass3Component } from './components/masterclass3/masterclass3.component';
import { ChoosemasterComponent } from './components/choosemaster/choosemaster.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgStreamingModule } from 'videogular2/compiled/streaming';
@NgModule({
  declarations: [Masterclass1Component, Masterclass2Component, Masterclass3Component, ChoosemasterComponent],
  imports: [
    CommonModule,
    MasterClassRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule
  ]
})
export class MasterClassModule { }
