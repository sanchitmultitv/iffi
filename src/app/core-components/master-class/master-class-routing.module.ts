import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChoosemasterComponent } from './components/choosemaster/choosemaster.component';
import { Masterclass1Component } from './components/masterclass1/masterclass1.component';
import { Masterclass2Component } from './components/masterclass2/masterclass2.component';
import { Masterclass3Component } from './components/masterclass3/masterclass3.component';
import { MasterClassComponent } from './master-class.component';


const routes: Routes = [
  {path:'', component:MasterClassComponent,
children:[
  {path:'', redirectTo:'choose'},
  {path:'class1', component:Masterclass1Component},
  {path:'class2', component:Masterclass2Component},
  {path:'class3', component:Masterclass3Component},
  {path:'choose', component:ChoosemasterComponent}
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterClassRoutingModule { }
