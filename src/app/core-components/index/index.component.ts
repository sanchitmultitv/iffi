import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  @ViewChild('myVideo', { static: true }) myVideo: ElementRef;
  videoEnd = false;
  constructor() { }

  ngOnInit(): void {
    this.playevent();
  }

  playevent(){
    let vid: any = document.getElementById("myVideo");
    vid.addEventListener('ended',(myHandler)=>{
      this.videoEnd = true;
    },false);
  }
    
}
