import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontAuditoriumDeskComponent } from './front-auditorium-desk.component';

describe('FrontAuditoriumDeskComponent', () => {
  let component: FrontAuditoriumDeskComponent;
  let fixture: ComponentFixture<FrontAuditoriumDeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontAuditoriumDeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontAuditoriumDeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
