import { Component, OnInit} from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {FetchDataService} from '../../../../services/fetch-data.service'

import { EventEmitter } from 'events';
import { GroupChatComponent } from '../../../../layout/group-chat/group-chat.component';
declare var $: any;
@Component({
  selector: 'app-left-auditorium',
  templateUrl:'./left-auditorium.component.html',
  styleUrls: ['./left-auditorium.component.scss'],
  animations: [fadeAnimation],
  providers:[GroupChatComponent]
})
export class LeftAuditoriumComponent implements OnInit {
  videoEnd = false;
  videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  // videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/video/networking_lounge_video.mp4';
  constructor(private gc:GroupChatComponent) { }

  ngOnInit(): void {
    console.log(this.videoPlayer,'this is 1');

  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat(){
    $('.groupchatsModal').modal('show')
    this.gc.loadData();
  }
}