import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-two',
  templateUrl: './auditorium-two.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})
export class AuditoriumTwoComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_17';
  serdia_room = localStorage.getItem('serdia_room');
  msg;
  qaList;
  exhibiton:any=[];
  documents:any=[];
  interval;
  exhibition_id = '2';
  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  videoEnd = false;
  //videoPlayerTwo = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream1.smil/playlist.m3u8';
  //videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/video/networking_lounge_video.mp4';

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    // this.chatGroupTwo();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 3000);
    console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.getQA();
    this.chatService.getconnect('toujeo-135');
    this.chatService.getMessages().subscribe((data => {
       console.log('data',data);
      if (data == 'groupchat') {
        this.chatGroup();
      }

    }));
  } 
  getHeartbeat(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '131');
    formData.append('audi', '75');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatTwo').modal('show')
    this.loadData();
  }

  loadData(): void {
    this.chatGroup();
    this.chatService.getconnect('toujeo-52');
    // this.chatService.getMessages().subscribe((data => {
    //   if (data == 'group_chat') {
    //     this.chatGroupTwo();
    //   }
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    // this.chatService
    //   .receiveMessages(this.serdia_room)
    //   .subscribe((msgs: any) => {
    //     if (msgs.roomId === 2){
    //       this.messageList.push(msgs);
    //     }        
    //     console.log('demo', this.messageList);
    //   });
  }
 chatGroup() {
    this._fd.groupchatingthree('iffi_2').subscribe(res => {
      console.log('two', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatThree').modal('hide');
  }
  room = 'iffi_2';
  postMessageTwo(value) {
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let data = JSON.parse(localStorage.getItem('virtual'));
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
    // this.chatService.sendMessage(value, data.name, this.serdia_room);
    this._fd.postGroupchat(value,data.name,data.email,this.room,created,data.id).subscribe(res=>{
      console.log('postdtaa');
      let arr ={
        "user_name": data.name,
        "chat_data": value
      };
      this.messageList.push(arr);
    });
    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  getQA(){
    //  console.log('exhibitonid',this.exhibition_id);
      let data = JSON.parse(localStorage.getItem('virtual'));
     // console.log('uid',data.id);
      this._fd.gethelpdeskanswers(data.id,this.exhibition_id).subscribe((res=>{
        //console.log(res);
        this.qaList = res.result;
        // alert('hello');
      }))
  
    }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
  //  console.log(value, data.id);
  // this.getQA();
    this._fd.helpdesk(data.id,value,this.exhibition_id).subscribe((res=>{
      //console.log(res);
      // this.getQA();
      if(res.code == 1){
        this.textMessage.reset();
        let arr ={
          "question": value,
          "answer": ""
        };
        this.qaList.push(arr);
        //this.toastr.success( '!');
      // var d = $('.chat_message');
      // d.scrollTop(d.prop("scrollHeight"))
      }
      //this.getQA();
       
    //  setTimeout(() => {
     //   $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
      //  this.msg = '';
//$('.liveQuestionModal').modal('hide');
  //    }, 2000);
      // setTimeout(() => {
      //   this.msg = '';
      //   $('.liveQuestionModal').modal('hide');
      // }, 2000);
     // this.textMessage.reset();
    }))
    

  }
}
