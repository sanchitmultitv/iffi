import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy {
  videoEnd = false;
 // videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  interval;
  //videoPlayer;
  //textMessage = new FormControl('');
  msg;
  qaList;
  exhibiton:any=[];
  documents:any=[];
  userid;
  exhibition_id = '79';
  like = false;
  socketMsg;
  liveMsg = false; 
   videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session1.smil/playlist.m3u8';
  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.userid = data.id;
   // this.getHeartbeat();
   this._fd.activeAudi().subscribe(res=>{
     //let datat = res.result[1].stream;
    //this.videoPlayer = res.result[1].stream;
    console.log('stream', this.videoPlayer);
   });
   this.getQA();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
    console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-135');
    this.chatService.getMessages().subscribe((data => {
       console.log('data',data);
       let check = data.split('_');
       if(check[0]=="start" && check[1]=="live"){
         this.liveMsg = true;
         console.log(check[2]);
         this.socketMsg = check[2];
       }
       if(check[0] == 'stop' && check[1]== 'live'){
        this.liveMsg = false;
       }
     console.log(check);
     if(check[0]=="question" && check[1]=="reply" && check[2]==this.userid && check[3]=="79"){
      this.getQA();
     }
      if (data == 'groupchat') {
        this.chatGroup();
      }

    }));
  }
  videoEnded() {
    this.videoEnd = true;
  }
  closePopuphelp(){
    $('.helpdeskaudi1').modal('hide');
  }
  closePopupgroup(){
    $('.groupchatOne').modal('hide');
  }
  likeopen(){
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  getHeartbeat(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '135');
    formData.append('audi', '79');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_16';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-135');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchatingthree('iffi_1').subscribe(res => {
      console.log('one', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatThree').modal('hide');
  }
  room = 'iffi_1';
  postMessage(value) {
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let data = JSON.parse(localStorage.getItem('virtual'));
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
    // this.chatService.sendMessage(value, data.name, this.serdia_room);
    this._fd.postGroupchat(value,data.name,data.email,this.room,created,data.id).subscribe(res=>{
      console.log('postdtaa');
      let arr ={
        "user_name": data.name,
        "chat_data": value
      };
      this.messageList.push(arr);
    });
    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  getQA(){
    //  console.log('exhibitonid',this.exhibition_id);
      let data = JSON.parse(localStorage.getItem('virtual'));
     // console.log('uid',data.id);
      this._fd.gethelpdeskanswers(data.id,this.exhibition_id).subscribe((res=>{
        //console.log(res);
        this.qaList = res.result;
        // alert('hello');
      }))
  
    }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
  //  console.log(value, data.id);
  // this.getQA();
    this._fd.helpdesk(data.id,value,this.exhibition_id).subscribe((res=>{
      //console.log(res);
      // this.getQA();
      if(res.code == 1){
        this.textMessage.reset();
        let arr ={
          "question": value,
          "answer": ""
        };
        this.qaList.push(arr);
        //this.toastr.success( '!');
      // var d = $('.chat_message');
      // d.scrollTop(d.prop("scrollHeight"))
      }
      //this.getQA();
       
    //  setTimeout(() => {
     //   $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
      //  this.msg = '';
//$('.liveQuestionModal').modal('hide');
  //    }, 2000);
      // setTimeout(() => {
      //   this.msg = '';
      //   $('.liveQuestionModal').modal('hide');
      // }, 2000);
     // this.textMessage.reset();
    }))
    

  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}

