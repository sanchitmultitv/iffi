import { Component, OnInit } from '@angular/core';
import { GroupChatFourComponent } from '../../../../layout/group-chat-four/group-chat-four.component';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-auditoriumfour',
  templateUrl: './auditoriumfour.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})
export class AuditoriumfourComponent implements OnInit {

  videoEnd = false;
  videoPlayerFour = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatFour').modal('show')
    this.loadData();
  }
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_19';
  serdia_room = localStorage.getItem('serdia_room');

  loadData(): void {
    this.chatGroup();
    this.chatService.getconnect('toujeo-52');
    // this.chatService.getMessages().subscribe((data=>{                 
    //   if(data == 'group_chat'){
    //     this.chatGroup();
    //   }           
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages(this.serdia_room)
      .subscribe((msgs: any) => {
        if (msgs.roomId === 4) {
          this.messageList.push(msgs);
        } 
        console.log('demo', this.messageList);
      });
  }
  chatGroup() {
    this._fd.groupchatingfour().subscribe(res => {
      // console.log('res',res); 

      // setTimeout(() => {
      //   $('#chat_messagedd')[0].scrollTop = $('#chat_messagedd')[0].scrollHeight;
      // }, 2000);
      this.messageList = res.result;
    });

  }
  closePopup() {
    $('.groupchatFour').modal('hide');
  }
  postMessageFour(value) {
    console.log(value);

    let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(data);

    this.chatService.sendMessage(value, data.name, this.serdia_room);
    // console.log(this.roomName);

    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);

  }
}


