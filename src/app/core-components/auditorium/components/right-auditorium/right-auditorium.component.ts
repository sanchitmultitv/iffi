import { Component, OnInit } from '@angular/core';
import { GroupChatTwoComponent } from '../../../../layout/group-chat-two/group-chat-two.component';
declare var $:any;
@Component({
  selector: 'app-right-auditorium',
  templateUrl: './right-auditorium.component.html',
  styleUrls: ['./right-auditorium.component.scss'],
  providers:[GroupChatTwoComponent]
})
export class RightAuditoriumComponent implements OnInit {
  videoEnd = false;
  videoPlayerTwo = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream1.smil/playlist.m3u8';
  //videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/video/networking_lounge_video.mp4';
  constructor(private gc:GroupChatTwoComponent) { }

  ngOnInit(): void {
    console.log(this.videoPlayerTwo,'this is 2');

  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat(){
    $('.groupchatsModalTwo').modal('show')
    this.gc.loadData();
  }
}
