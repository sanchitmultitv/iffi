import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditoriumRoutingModule } from './auditorium-routing.module';
import { AuditoriumComponent } from './auditorium.component';
import { LeftAuditoriumComponent } from './components/left-auditorium/left-auditorium.component';
import { RightAuditoriumComponent } from './components/right-auditorium/right-auditorium.component';
import { KbcQuizComponent } from './components/kbc-quiz/kbc-quiz.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgStreamingModule } from 'videogular2/compiled/streaming';
import { FrontAuditoriumComponent } from './components/front-auditorium/front-auditorium.component';
import { FrontAuditoriumDeskComponent } from './components/front-auditorium-desk/front-auditorium-desk.component';
import { FourthAuditoriumComponent } from './components/fourth-auditorium/fourth-auditorium.component';
import { GroupChatComponent } from '../../layout/group-chat/group-chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GroupChatTwoComponent } from '../../layout/group-chat-two/group-chat-two.component';
import { GroupChatThreeComponent } from '../../layout/group-chat-three/group-chat-three.component';
import { GroupChatFourComponent } from '../../layout/group-chat-four/group-chat-four.component';
import { ChooseAudiComponent } from './components/choose-audi/choose-audi.component';

@NgModule({
  declarations: [AuditoriumComponent, LeftAuditoriumComponent, RightAuditoriumComponent, KbcQuizComponent, FrontAuditoriumComponent, FrontAuditoriumDeskComponent, FourthAuditoriumComponent, ChooseAudiComponent,],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuditoriumRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule
  ],
  entryComponents:[GroupChatComponent, GroupChatComponent, GroupChatTwoComponent, GroupChatThreeComponent, GroupChatFourComponent]
})
export class AuditoriumModule { }
