import { FetchDataService } from './../../services/fetch-data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit {
  videoEnd=false;
  liveMsg= false;
  ChatMsg = false;
  senderName;
  //videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/video/networking_lounge_video.mp4';
  totalscore=0;  
  result: any;
  submitanswers = {};
  // userid: any = JSON.parse(localStorage.getItem('virtual')).id;
  userid: any = '20'
  selectedArray: any = [];
  showQuiz = false;
  socketMsg;
  constructor(private router: Router, private chat: ChatService, private _fd:FetchDataService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-135');
    this.chat.getMessages().subscribe((data=>{
    //  console.log('data',data);
    console.log('data',data);
    let check = data.split('_');
    if(check[0]=="start" && check[1]=="live"){
      this.liveMsg = true;
      console.log(check[2]);
      this.socketMsg = check[2];
    }
    if(check[0] == 'stop' && check[1]== 'live'){
      this.liveMsg = false;
     }
      else{
        let getMsg = data.split('_');
        console.log('chats',getMsg);
        if(getMsg[0] == "one" && getMsg[1] == "to" && getMsg[2] == "one"){
          let data = JSON.parse(localStorage.getItem('virtual'));
          if(getMsg[3]== data.id){
            this.senderName = getMsg[4];
            this.ChatMsg = true;
            setTimeout(() => {
              this.ChatMsg = false;
            }, 5000);
          }
        }
      }
    }));
    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime=0;
        pauseVideo.pause();
      }
    }
  }
  openLevels() {
    $('.showlevels').modal('show')

  }
  openGroupChats() {
    this.showQuiz = true;
    $('.groupchatFours').modal('show');
    // this.getLevel1();

  }
  level=1;
  closetriiva(){
    $('.showlevels').modal('hide');
  }
  closePopupquiz(){
    $('.groupchatFours').modal('hide');
  }
  getLevel(level) {
    this._fd.getQuestions(level).subscribe((res: any) => {
      this.selectedArray=[];
      if(level===55){
        this.level=1;
      }else{
        this.level=2;
      }
      this.result = res.result;
      this.openGroupChats();
      res.result.forEach(element => {
        this.selectedArray.push({ "quiz_id": element.id, "answer": '',  });
      });
    })
  }

  submit() {
    let raw: any = JSON.stringify(this.selectedArray);
    this.totalscore=0;
    for (let i = 0; i < this.result.length; i++) {
      if(this.selectedArray[i].answer===this.result[i].correct_answer){
        this.totalscore=this.totalscore+1;
      }
    }
    const formData = new FormData();
    formData.append('user_id', this.userid);
    formData.append('data', raw);
    this.showQuiz = false;
    this._fd.onsubmitquiz(formData).subscribe((res: any) => {
      console.log(res, 'res');
      // this.level=2;
      // if (res.code === 1) {

      // }else{

      // }
    });
  }

  getAnswers(id, answer) {
    // this.submitanswers = Object.assign()
    let arr = [];
    arr.push(id, answer);
    for (let i = 0; i < this.result.length; i++) {
      if (this.selectedArray[i].quiz_id === id) {
        this.selectedArray[i].answer = answer;
      }
    }
    this.submitanswers = { user_id: this.userid, data: this.selectedArray };
    // console.log(id, answer, this.submitanswers);
  }

  showVideoPopup(){
    $('#playVideo').modal('show');
    let player: any = document.getElementById("video");
    player.play();
  }
  closeModalVideo(){
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime=0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  videoEnded() {
    this.videoEnd = true;
  }
  openWtsapp(){
    $('.wtsappModal').modal('show');
  }
  openChat(){
    $('.chatsModal').modal('show');
  }
  openCamera(){
    this.router.navigate(['/capturePhoto']);
  }

  
}
