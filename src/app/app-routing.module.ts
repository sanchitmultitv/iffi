import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnvideoComponent } from './onvideo/onvideo.component';
import { ThanksComponent } from './thanks/thanks.component';

const routes: Routes = [
  { path: '', redirectTo: 'first', pathMatch: 'prefix' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
  {path:'thanks', loadChildren:() =>import('../app/thanks/thanks.module').then(m => m.ThanksModule)},
  { path: '', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule) },
  {path:'firstPage',  loadChildren:() =>import('./first/first.module').then(m => m.FirstModule)},
  {path:'first', component:OnvideoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {'useHash': true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
