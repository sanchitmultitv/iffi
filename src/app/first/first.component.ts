import { Component, OnInit,HostListener } from '@angular/core';
import { fadeAnimation } from '../shared/animation/fade.animation';
@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss'],
  animations: [fadeAnimation]
})
export class FirstComponent implements OnInit {
  landscape = true;
  constructor() { }

  ngOnInit(): void {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else{
      this.landscape = true;
    }
  }

}
