import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaModule } from 'angular-google-recaptcha';
import { NgxCaptchaModule } from 'ngx-captcha';


@NgModule({
  declarations: [
    SignupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SignupRoutingModule,
    NgxCaptchaModule,
    RecaptchaModule.forRoot({
      siteKey: '6Le4lyEaAAAAAP7paHtxbMd01qwNQV8SaVA9rXEr',
  }),
  ]
})
export class SignupModule { }
