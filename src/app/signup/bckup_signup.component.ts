import { HttpClient } from '@angular/common/http';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
declare var Razorpay: any;
declare var $:any;
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf('@') != -1) {
    let [_, domain] = email.split('@');
    if (domain !== 'sanofi.com' && domain !== 'sanofi-india.com') {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;
  options: any = [];
  response_check:any;
  order_history:any;
  vaeify_pay:any;
  submitted = false;
  fileToUpload: File = null;
  paidWeblink: File = null;
  pressID: File = null;
  selfDeclaration: File = null;
  err=false;
  siteKey= '6Le4lyEaAAAAAP7paHtxbMd01qwNQV8SaVA9rXEr';
  checkMessage = 'Please click on agree to Register';
  signupForm = new FormGroup({
    first_name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email,
      Validators.pattern('[^ @]*@[^ @]*'),
      emailDomainValidator]),
    job_title: new FormControl('', [Validators.required]),
  });

  public imagePath;
  imgURL: any;
  public message: string;
  msg;
  colr;
  uname;
  uemail;
  profileForm:FormGroup;
  aFormGroup: FormGroup;
  user__id;
  btn_check=false;
  myRecaptcha = new FormControl(false);
  type_media= false;
  freelance_media = false;
  paid_media= false;
  constructor(private router: Router, private _fd: FetchDataService, private _auth: AuthService, private http:HttpClient,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
   // $('.thanks').modal('show');
   // formbuilder Validations
   if (window.innerHeight > window.innerWidth) {
    console.log('ca',this.myRecaptcha.value);
    $('.ng-tns-c0-0').children().hide();
    $('.ng-tns-c0-0').children().show();
  }
  
this.profileForm = this.formBuilder.group({
  name: ['', [Validators.required,Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$')]],
  email: ['', [Validators.required, Validators.email,Validators.pattern('[^ @]*@[^ @]*')]],
  age: ['', Validators.required],
  sex: ['', Validators.required],
  yourself: ['', Validators.required],
  user_type: ['Normal', Validators.required],
 // recaptcha: ['', Validators.required],
  phone : ['', Validators.required],
  media_type : ['Paid'],
  // first_image:[''],
  // weblink_image:[''],
  // weblink1:[''],
  // weblink2:[''],
  // weblink3:[''],
  // press_id:[''],
  // freelance_image:[''],
  // freelance_weblink:['']
  
  
});
  }
  onOptionsSelected(getvalue){
   if(getvalue == 'press'){
     this.type_media = true;
     this.paid_media = true;
     this.farr.forEach(element => {
      this.profileForm.addControl(element, new FormControl('', Validators.required));

    });
   }
   if(getvalue == 'Normal'){
    this.type_media = false;
    this.paid_media = false;
    this.freelance_media = false;
  }
  }
  farr=[ ];
  farraytwo=[];


  getMediaOrganisation(getMedia){
    this.farr=[];
    this.farraytwo=[];
    this.farr=["first_image","press_id"];
    this.farraytwo=["weblink_image", "weblink1","weblink2","weblink3"];

    if(getMedia == 'freelance'){
      this.freelance_media = true;
      this.paid_media = false;
      this.farr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.farraytwo.forEach(element => {
        this.profileForm.removeControl(element);
  
      });

    }
    if(getMedia == 'Paid'){
      this.freelance_media = false;
      this.paid_media = true;
      this.farr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      this.farraytwo.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      
      // this.profileForm.addControl('weblink1', new FormControl('', Validators.required)); 
      // this.profileForm.addControl('press_id', new FormControl('', Validators.required));
      //this.profileForm.addControl('first_image', this.formBuilder.control('', [Validators.required]));
    }
    console.log('cqals',this.profileForm.controls);

    
  }
  handleLoad() {
    console.log(this.myRecaptcha.value);
    console.log('Google reCAPTCHA loaded and is ready for use!')
}
handleSuccess(value){
  console.log(value, "values");
  if(value ! = '' || value != undefined){
  this.btn_check = true;
  }
  
}
handleFileInput(event) {
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    console.log(file, file)
    this.fileToUpload = file;
    this.err = true;
    // this.form.get('avatar').setValue(file);
  }
  // this.fileToUpload = files[0];
  // let file = new FileReader();
  // file.readAsDataURL(files)
  // console.log('change',files,   file.readAsDataURL(files)
  // )
}
handleweblinkPaid(files: FileList) {
  
  this.paidWeblink = files.item(0);
  console.log('change', this.fileToUpload)
}
handlePressId(files: FileList) {
  
  this.pressID = files.item(0);
  console.log('change', this.fileToUpload)
}
handleFreelanceMedia(files: FileList) {
  
  this.selfDeclaration = files.item(0);
  console.log('change', this.selfDeclaration)
}
onScriptError() {
    console.log('Something went long when loading the Google reCAPTCHA')
}
resolved(captchaResponse: string) {
  console.log(`Resolved captcha with response: ${captchaResponse}`);
}
check(){
console.log('ghjk')
}
  preview(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    } else {
      const file = files[0];
      this.signupForm.patchValue({
        image: file
      });
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
 // convenience getter for easy access to form fields
 get f() { return this.profileForm.controls; }

  isChecked(event) {
    this.checked = !this.checked;
  }
  register() {
    this.submitted = true;
    console.log(this.profileForm.value);
    console.log('file', this.fileToUpload)
    if (this.profileForm.invalid) {
      return;
  }
    console.log(this.profileForm.value);
    this.uname = this.profileForm.value.name;
    this.uemail = this.profileForm.value.email;
    const formData = new FormData();
    formData.append('name', this.profileForm.value.name);
    formData.append('email', this.profileForm.value.email);
    formData.append('age', this.profileForm.value.age);
    formData.append('user_type', this.profileForm.value.user_type);
    formData.append('about', this.profileForm.value.yourself);
    formData.append('sex', this.profileForm.value.sex);
    formData.append('mobile', this.profileForm.value.phone);
    formData.append('media_type', this.profileForm.value.media_type);
    formData.append('text1', this.profileForm.value.weblink1);
    formData.append('text2', this.profileForm.value.weblink2);
    formData.append('text3', this.profileForm.value.weblink3);
    formData.append('text4', this.profileForm.value.freelance_weblink);
    formData.append('image1', this.fileToUpload);
    formData.append('image2', this.paidWeblink);
    formData.append('image3', this.pressID);
    formData.append('image4', this.selfDeclaration);
    
    const user ={
      name:this.profileForm.value.name,
      email: this.profileForm.value.email,
      age: this.profileForm.value.age,
      user_type: this.profileForm.value.user_type,
      about: this.profileForm.value.yourself,
      sex: this.profileForm.value.sex,
      phone: this.profileForm.value.phone
    };
    localStorage.setItem('user_data', JSON.stringify(user));
      // tslint:disable-next-line:align
      
      this._auth.register(formData).subscribe((res:any) => {

        if(res.code == '1' && this.profileForm.value.user_type == 'Normal'){
          this.submitted = false;
          let user_id = res.id;
          this.user__id = res.id;
          this.msg = '';
          this.http.get('https://virtualapi.multitvsolution.com/ifii_backend/create_orders.php?user_id=' + user_id).subscribe(
            (res: Response) => {
              console.log(res);
              this.response_check = res;
              if(this.response_check.status == 'success') {
                this.options = {
                  'key': 'rzp_live_1K7ay6wAMdzD5y', // Enter the Key ID generated from the Dashboard
                  'amount': 59000, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                  'currency': 'INR',
                  'name': 'Ministry of I&B',
                  'description': 'Ministry of I&B.',
                  'image': 'https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/logo_iffi.jpg',
                  'order_id': this.response_check.order_id, // This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                  'handler':  (response) => {
                    console.log(response.razorpay_payment_id);
                    console.log(response.razorpay_order_id);
                    console.log(response.razorpay_signature);
                    this.vaeify_pay = response;
                    this.verify();

                },
                  'prefill': {
                      'name':  this.uname,
                      'email': this.uemail,
                      // "contact": "9999999999"
                  },
                  'notes': {
                      'address': 'Razorpay Corporate Office'
                  },
                  'theme': {
                      'color': '#3399cc'
                  }
              };
              var rzp1 = new Razorpay(this.options);
             // document.getElementById('rzp-button1').onclick = function(e){
                  rzp1.open();
                  // e.preventDefault();
                  
              }
              

       
      });
        } else {
          this.msg = res.result;
        }
        this.profileForm.reset();
      });
  
// hello(res){
//   console.log(this.detail);
//   console.log(res);

//  let order_detail = {
//     razorpay_order_id:res.razorpay_order_id,
//     razorpay_payment_id:res.razorpay_payment_id,
//     amount:this.amounts/100,
//     email:this.emails,
//     contact:this.contacts,
//     name:this.names,
//     item:this.allfood,
//     address:this.addresss,
//     locality:this.localities,
//     state:this.states
//   }
      
//   this.http.post('https://app.firstfiddle.in/public/diypayment',order_detail).subscribe(
//       (res2:Response) => {
//         this.lol = res2;
//         console.log(this.lol);
//       }
// )

// }
  }
//   ngAfterViewInit(): void
// {
//   $('.ng-tns-c0-0').children().hide();
//   $('.ng-tns-c0-0').children().show(); 
// }
  verify(){
   // alert('baba');
    const paymentdata = new FormData();
    paymentdata.append('razorpay_order_id', this.vaeify_pay.razorpay_order_id);
    paymentdata.append('razorpay_payment_id', this.vaeify_pay.razorpay_payment_id);
    paymentdata.append('razorpay_signature', this.vaeify_pay.razorpay_signature);
    paymentdata.append('user_id', this.user__id);
    this.http.post('https://virtualapi.multitvsolution.com/ifii_backend/verify/verify.php', paymentdata).subscribe( 
     ((res:any) => {
       console.log('payment_verify response', res);
       if(res.status == 'success'){
        const statusData = new FormData();
        statusData.append('id', this.user__id);
        statusData.append('status', '1');
         // tslint:disable-next-line:align
         this._auth.updateStatus(statusData).subscribe(res => {
           console.log(res);
         });


        const mailData = new FormData();
        mailData.append('name', this.uname);
        mailData.append('email', this.uemail);
        this._auth.successMail(mailData).subscribe(res => {
           console.log(res);
         });
        $('.thanks').modal('show');
       }
  }));
  }
  closePopup(){
$('.thanks').modal('hide');
  }
}
