import { HttpClient } from '@angular/common/http';
import { Component, OnInit, AfterViewInit,OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
import { flatten } from 'underscore';
declare var Razorpay: any;
declare var $:any;
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf('@') != -1) {
    let [_, domain] = email.split('@');
    if (domain !== 'sanofi.com' && domain !== 'sanofi-india.com') {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;
  options: any = [];
  response_check:any;
  order_history:any;
  vaeify_pay:any;
  submitted = false;
  fileToUpload: File = null;
  paidWeblink: File = null;
  pressID: File = null;
  selfDeclaration: File = null;
  weblink_image= false;
  press_idbool= false;
  err = false;
  student = false;
  studentid_bool = false;
  govid_bool = false;
  stletter_bool = false;
  genral=true;
  genral_bool = false;
  paid_weblinks = false;
  bylines=false;
  weblinks = false;
  line1bool = false;
  line2bool = false;
  line3bool = false;
  web1bool = false;
  web2bool = false;
  web3bool = false;
  freelance_weblinks = false;
  bylinesfree = false
  weblinks_free = false;
  siteKey= '6Le4lyEaAAAAAP7paHtxbMd01qwNQV8SaVA9rXEr';
  checkMessage = 'Please click on agree to Register';
  signupForm = new FormGroup({
    first_name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email,
      Validators.pattern('[^ @]*@[^ @]*'),
      emailDomainValidator]),
    job_title: new FormControl('', [Validators.required]),
  });

  public imagePath;
  imgURL: any;
  public message: string;
  msg;
  colr;
  uname;
  uemail;
  profileForm:FormGroup;
  aFormGroup: FormGroup;
  user__id;
  btn_check=false;
  myRecaptcha = new FormControl(false);
  type_media= false;
  freelance_media = false;
  frelnceLetter = false;
  paid_media= false;
  free1bool =false;
  free2bool =false;
  free3bool =false;
  free4bool =false;
  free5bool =false;
  general_govid :File = null;
  recommnedation_letter:File = null;
  byLines1 :File = null;
  byLines2 :File = null;
  byLines3 :File = null;
  byLines4 :File = null;
  byLines5 :File = null;
  press_ID : File = null;
  constructor(private router: Router, private _fd: FetchDataService, private _auth: AuthService, private http:HttpClient,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.test();
    
    
   // $('.thanks').modal('show');
   // formbuilder Validations
  //  if (window.innerHeight > window.innerWidth) {
  //   console.log('ca',this.myRecaptcha.value);
  //   $('.ng-tns-c0-0').children().hide();
  //   $('.ng-tns-c0-0').children().show();
  // }
  
this.profileForm = this.formBuilder.group({
  name: ['', [Validators.required,Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$')]],
  email: ['', [Validators.required, Validators.email,Validators.pattern('[^ @]*@[^ @]*')]],
  age: ['', Validators.required],
  sex: ['', Validators.required],
  yourself: ['', Validators.required],
  user_type: ['Normal', Validators.required],
 // recaptcha: ['', Validators.required],
  phone : ['', Validators.required],
  media_type : ['Paid'],
  
  genral_copy:['', Validators.required],
 // line_types:[''],
  // weblink1:[''],
  // weblink2:[''],
  // weblink3:[''],
  // press_id:[''],
  // freelance_image:[''],
  // freelance_weblink:['']
  
  
});
console.log(this.profileForm.controls);
  }

  test(){
    if(this.router.url == '/signup'){
      // alert('hi')
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/60016c1aa9a34e36b96cb614/1es2osib9';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
      }
      // else{
      //   alert(this.router.url);
      // //document.getElementById('tawkchat-minified-box').style.display = "none";
      
      // }
  }


  freeweb1bool = false;
  paidArr = ["first_image",  "press_id"];
  bylinesArr = ["line1", "line2", "line3"];
  weblinksArr = ["weblink1","weblink2", "weblink3"];
  freelance_linesArr = ["free1", "free2", "free3", "free4", "free5"];
  freeweblinks = ["freelance_weblink"];
  //paidntValid = [ "weblink_image", "weblink2", "weblink3"];

  frelanceArr = ["freelance_image"];
  studentArr = ["student_image", "student_govid", "student_letter"];
  onOptionsSelected(getvalue){
   if(getvalue == 'press'){
    this.paid_weblinks = true;
    this.freelance_media = false;
    this.weblinks_free = false;
    this.freelance_weblinks = false;
     this.genral = false;
     this.type_media = true;
     this.student = false;
     this.paid_media = true;
     this.profileForm.removeControl('genral_copy');
     this.paidArr.forEach(element => {
      this.profileForm.addControl(element, new FormControl('', Validators.required));

    });
    // this.paidntValid.forEach(element => {
    //   this.profileForm.addControl(element, new FormControl(''));

    // });
    this.studentArr.forEach(element => {
      this.profileForm.removeControl(element);
    
    });
    console.log(this.profileForm.controls);
   }
   if(getvalue == 'Normal'){
     this.paid_media = false
     this.paid_weblinks = true;
     this.freelance_media = false;
     this.weblinks_free = false;
     this.freelance_weblinks = false;
     this.genral = true;
    this.type_media = false;
    this.student = false;
    this.paid_media = false;
    this.freelance_media = false;
    this.profileForm.addControl("genral_copy", new FormControl('', Validators.required));
    this.frelanceArr.forEach(element => {
      this.profileForm.removeControl(element);

    });
    this.paidArr.forEach(element => {
      this.profileForm.addControl(element,new FormControl('', Validators.required));

    });
    // this.paidntValid.forEach(element => {
    //   this.profileForm.removeControl(element);

    // });
    this.studentArr.forEach(element => {
      this.profileForm.removeControl(element);
    
    });
  }

  if(getvalue == 'student'){
    this.paid_weblinks = false;
    this.paid_media = false;
    this.freelance_media = false;
    this.weblinks_free = false;
    this.freelance_weblinks = false;
    this.genral = false;
this.student = true;
this.type_media = false;
this.paid_media = false;
this.freelance_media = false;
this.profileForm.removeControl('genral_copy');
this.studentArr.forEach(element => {
  this.profileForm.addControl(element, new FormControl('', Validators.required));

});
this.paidArr.forEach(element => {
  this.profileForm.removeControl(element);

});
// this.paidntValid.forEach(element => {
//   this.profileForm.removeControl(element);

// });
this.frelanceArr.forEach(element => {
  this.profileForm.removeControl(element);

});
console.log(this.profileForm.controls);
  } 
  }

  getMediaOrganisation(getMedia){
    if(getMedia == 'freelance'){
      this.bylines = false;
      this.weblinks =false;
      this.freelance_weblinks = true;
      this.paid_weblinks = false;
      this.genral = false;
      this.freelance_media = true;
      this.paid_media = false;
      this.profileForm.removeControl('genral_copy');
      this.frelanceArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });

      console.log(this.profileForm.controls);
      this.paidArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      // this.paidntValid.forEach(element => {
      //   this.profileForm.removeControl(element);
  
      // });
    }
    if(getMedia == 'Paid'){
      this.freelance_weblinks = false;
      this.bylinesfree = false;
      this.weblinks_free = false;
      this.paid_weblinks = true;
      this.genral= false;
      this.freelance_media = false;
      this.paid_media = true;
      this.profileForm.removeControl('genral_copy');
      this.paidArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      // this.paidntValid.forEach(element => {
      //   this.profileForm.addControl(element, new FormControl(''));
  
      // });
      this.frelanceArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      console.log(this.profileForm.controls);
    }
    
  }

  getweblinkstype_paid(getdata){
    console.log(getdata);
    if(getdata == ''){
      this.bylines = false;
      this.weblinks=false;
    }
    if(getdata == 'byliness'){
      this.bylines = true;
      this.weblinks=false;
      this.genral = false;
      this.freelance_media = false;
      this.paid_media = true;
      this.line1bool = false;
      this.line2bool = false;
      this.line3bool = false;
      console.log(this.profileForm.controls);
      this.profileForm.removeControl('genral_copy');
      this.frelanceArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });

     
      this.paidArr.forEach(element => {
        this.profileForm.addControl(element,new FormControl('', Validators.required));
  
      });
      this.bylinesArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      this.weblinksArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
    }

    if(getdata == 'weblinkss'){
      this.bylines =false;
      this.weblinks = true;
      this.genral= false;
      this.freelance_media = false;
      this.paid_media = true;
      this.profileForm.removeControl('genral_copy');
      this.paidArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      this.bylinesArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.weblinksArr.forEach(element => {
       
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      // this.paidntValid.forEach(element => {
      //   this.profileForm.addControl(element, new FormControl(''));
  
      // });
      this.frelanceArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      console.log(this.profileForm.controls);
    }
    
  }
  getweblinkstype_freelance(getdata){
    console.log(getdata);
    if(getdata == ''){
      this.bylines = false;
      this.weblinks=false;
      this.bylinesfree = false;
      this.weblinks_free = false
    }
    if(getdata == 'freelines'){
      this.bylinesfree = true;
      this.weblinks_free = false
      this.bylines = false;
      this.weblinks=false;
      this.genral = false;
      this.freelance_media = true;
      this.paid_media = false;
      this.line1bool = false;
      this.line2bool = false;
      this.line3bool = false;
      console.log(this.profileForm.controls);
      this.profileForm.removeControl('genral_copy');
      this.frelanceArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });

      this.freelance_linesArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      this.freeweblinks.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
     
      this.paidArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.bylinesArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.weblinksArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
    }

    if(getdata == 'freeelinkss'){
      this.bylinesfree = false;
      this.weblinks_free = true
      this.bylines =false;
      this.weblinks = false;
      this.genral= false;
      this.freelance_media = false;
      this.paid_media = false;
      this.profileForm.removeControl('genral_copy');
      this.frelanceArr.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });

      this.freelance_linesArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.freeweblinks.forEach(element => {
        this.profileForm.addControl(element, new FormControl('', Validators.required));
  
      });
      this.paidArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.bylinesArr.forEach(element => {
        this.profileForm.removeControl(element);
  
      });
      this.weblinksArr.forEach(element => {
       
        this.profileForm.removeControl(element);
  
      });
      // this.paidntValid.forEach(element => {
      //   this.profileForm.addControl(element, new FormControl(''));
  
      // });
     
      console.log(this.profileForm.controls);
    }
    
  }

  handleLoad() {
    console.log(this.myRecaptcha.value);
    console.log('Google reCAPTCHA loaded and is ready for use!')
}
handleSuccess(value){
  console.log(value, "values");
  if(value ! = '' || value != undefined){
  this.btn_check = true;
  }
  
}
handleFileInput(event) {
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    console.log(file, file)
    this.recommnedation_letter = file;
    this.err = true;
    // this.form.get('avatar').setValue(file);
  }
  // this.fileToUpload = files[0];
  // let file = new FileReader();
  // file.readAsDataURL(files)
  // console.log('change',files,   file.readAsDataURL(files)
  // )
}

handleGenralcopy(files: FileList) {
  
  this.general_govid = files.item(0);
  console.log('change', this.general_govid)
  this.genral_bool = true;
}
handleweblinkPaid(files: FileList) {
  
  this.paidWeblink = files.item(0);
  console.log('change', this.fileToUpload)
  this.weblink_image = true;
}
handleline1(files: FileList) {
  
  this.byLines1 = files.item(0);
  console.log('change', this.byLines1)
  this.line1bool = true;
}
handleline2(files: FileList) {
  
  this.byLines2 = files.item(0);
  console.log('change', this.byLines2)
  this.line2bool = true;
}
handleline3(files: FileList) {
  
  this.byLines3 = files.item(0);
  console.log('change', this.byLines3)
  this.line3bool = true;
}
handlefree_line1(files: FileList) {
  
  this.byLines1 = files.item(0);
  console.log('change', this.byLines1)
  this.free1bool = true;
}
handlefree_line2(files: FileList) {
  
  this.byLines2 = files.item(0);
  console.log('change', this.byLines2)
  this.free2bool = true;
}
handlefree_line3(files: FileList) {
  
  this.byLines3 = files.item(0);
  console.log('change', this.byLines3)
  this.free3bool = true;
}
handlefree_line4(files: FileList) {
  
  this.byLines4 = files.item(0);
  console.log('change', this.byLines4)
  this.free4bool = true;
}
handlefree_line5(files: FileList) {
  
  this.byLines5 = files.item(0);
  console.log('change', this.byLines5)
  this.free5bool = true;
}
handlePressId(files: FileList) {
  
  this.press_ID = files.item(0);
  console.log('change', this.press_ID);
  this.press_idbool = true;
}
handleFreelanceMedia(files: FileList) {
  
  this.recommnedation_letter = files.item(0);
  console.log('change', this.recommnedation_letter)
  this.frelnceLetter = true;
}
studentiss_id: File=null;
handlestudentID(files: FileList) {
  this.studentiss_id = files.item(0);
  console.log('change', this.studentiss_id)
  this.studentid_bool = true;
}
handleGovID(files: FileList) {
  
  this.general_govid = files.item(0);
  console.log('change', this.general_govid)
  this.govid_bool = true;
}
handleStudentLetter(files: FileList) {
  
  this.recommnedation_letter = files.item(0);
  console.log('change', this.recommnedation_letter)
  this.stletter_bool = true;
}



onScriptError() {
    console.log('Something went long when loading the Google reCAPTCHA')
}
resolved(captchaResponse: string) {
  console.log(`Resolved captcha with response: ${captchaResponse}`);
}
check(){
console.log('ghjk')
}
  preview(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    } else {
      const file = files[0];
      this.signupForm.patchValue({
        image: file
      });
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
 // convenience getter for easy access to form fields
 get f() { return this.profileForm.controls; }

  isChecked(event) {
    this.checked = !this.checked;
  }
  register() {
    this.submitted = true;
    console.log(this.profileForm.value);
    console.log('file', this.fileToUpload)
    if (this.profileForm.invalid) {
      return;
  }
    console.log(this.profileForm.value);
    this.uname = this.profileForm.value.name;
    this.uemail = this.profileForm.value.email;
    if(this.profileForm.value.user_type =="student"){
      this.profileForm.value.media_type = "";
    }
    const formData = new FormData();
    formData.append('name', this.profileForm.value.name);
    formData.append('email', this.profileForm.value.email);
    formData.append('age', this.profileForm.value.age);
    formData.append('user_type', this.profileForm.value.user_type);
    formData.append('about', this.profileForm.value.yourself);
    formData.append('sex', this.profileForm.value.sex);
    formData.append('mobile', this.profileForm.value.phone);
    if(this.profileForm.value.user_type == 'Normal'){
      this.profileForm.value.media_type = "";
    }
    formData.append('media_type', this.profileForm.value.media_type);
    formData.append('weblinks', this.profileForm.value.weblink1);
    formData.append('weblinks_2', this.profileForm.value.weblink2);
    formData.append('weblinks_3', this.profileForm.value.weblink3);
    formData.append('freelnace_weblink', this.profileForm.value.freelance_weblink);
    // formData.append('image1', this.fileToUpload);
    // formData.append('image2', this.paidWeblink);
    // formData.append('image3', this.pressID);
    //formData.append('image4', this.selfDeclaration);
    formData.append('goverment_issue_id', this.general_govid);
    formData.append('student_id', this.studentiss_id);
    formData.append('recommendation_letter', this.recommnedation_letter);
    formData.append('bylines_1', this.byLines1);
    formData.append('bylines_2', this.byLines2);
    formData.append('bylines_3', this.byLines3);
    formData.append('bylines_4', this.byLines4);
    formData.append('bylines_5', this.byLines5);
    formData.append('press_id', this.press_ID);
    
    const user ={
      name:this.profileForm.value.name,
      email: this.profileForm.value.email,
      age: this.profileForm.value.age,
      user_type: this.profileForm.value.user_type,
      about: this.profileForm.value.yourself,
      sex: this.profileForm.value.sex,
      phone: this.profileForm.value.phone
    };
    localStorage.setItem('user_data', JSON.stringify(user));
      // tslint:disable-next-line:align
      
      this._auth.register(formData).subscribe((res:any) => {

        if(res.code == '1' && this.profileForm.value.user_type == 'Normal'){
          this.submitted = true;
          let user_id = res.id;
          this.user__id = res.id;
          this.msg = '';
          this.http.get('https://virtualapi.multitvsolution.com/ifii_backend/create_orders.php?user_id=' + user_id).subscribe(
            (res: Response) => {
              console.log(res);
              this.response_check = res;
              if(this.response_check.status == 'success') {
                this.options = {
                  'key': 'rzp_live_1K7ay6wAMdzD5y', // Enter the Key ID generated from the Dashboard
                  'amount': 59000, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                  'currency': 'INR',
                  'name': 'Ministry of I&B',
                  'description': 'Ministry of I&B.',
                  'image': 'https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/logo_iffi.jpg',
                  'order_id': this.response_check.order_id, // This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                  'handler':  (response) => {
                    console.log(response.razorpay_payment_id);
                    console.log(response.razorpay_order_id);
                    console.log(response.razorpay_signature);
                    this.vaeify_pay = response;
                    this.verify();

                },
                  'prefill': {
                      'name':  this.uname,
                      'email': this.uemail,
                      // "contact": "9999999999"
                  },
                  'notes': {
                      'address': 'Razorpay Corporate Office'
                  },
                  'theme': {
                      'color': '#3399cc'
                  }
              };
              var rzp1 = new Razorpay(this.options);
             // document.getElementById('rzp-button1').onclick = function(e){
                  rzp1.open();
                  // e.preventDefault();
                  
              }
              

       
      });
        } 
        if(res.code == '1' && (this.profileForm.value.user_type == "press" ||this.profileForm.value.user_type == "student" )){
          this.msg = res.result;
          this.submitted = false;
         // $('.thanks').modal('show');
          setTimeout(() => {
            window.location.reload();
          }, 4000);
        }
        
        else {
          this.msg = res.result;
          this.submitted = false;
          //window.location.reload();
          
        }
        this.profileForm.reset();
      });
  
// hello(res){
//   console.log(this.detail);
//   console.log(res);

//  let order_detail = {
//     razorpay_order_id:res.razorpay_order_id,
//     razorpay_payment_id:res.razorpay_payment_id,
//     amount:this.amounts/100,
//     email:this.emails,
//     contact:this.contacts,
//     name:this.names,
//     item:this.allfood,
//     address:this.addresss,
//     locality:this.localities,
//     state:this.states
//   }
      
//   this.http.post('https://app.firstfiddle.in/public/diypayment',order_detail).subscribe(
//       (res2:Response) => {
//         this.lol = res2;
//         console.log(this.lol);
//       }
// )

// }
  }
//   ngAfterViewInit(): void
// {
//   $('.ng-tns-c0-0').children().hide();
//   $('.ng-tns-c0-0').children().show(); 
// }
  verify(){
   // alert('baba');
    const paymentdata = new FormData();
    paymentdata.append('razorpay_order_id', this.vaeify_pay.razorpay_order_id);
    paymentdata.append('razorpay_payment_id', this.vaeify_pay.razorpay_payment_id);
    paymentdata.append('razorpay_signature', this.vaeify_pay.razorpay_signature);
    paymentdata.append('user_id', this.user__id);
    this.http.post('https://virtualapi.multitvsolution.com/ifii_backend/verify/verify.php', paymentdata).subscribe( 
     ((res:any) => {
       console.log('payment_verify response', res);
       if(res.status == 'success'){
        const statusData = new FormData();
        statusData.append('id', this.user__id);
        statusData.append('status', '1');
         // tslint:disable-next-line:align
         this._auth.updateStatus(statusData).subscribe(res => {
           console.log(res);
         });


        const mailData = new FormData();
        mailData.append('name', this.uname);
        mailData.append('email', this.uemail);
        this._auth.successMail(mailData).subscribe(res => {
           console.log(res);
         });
        $('.thanks').modal('show');
       }
  }));
  }
  closePopup(){
$('.thanks').modal('hide');
  }
  ngOnDestroy(): void{
    this.test();
  }
}
