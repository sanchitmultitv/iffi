import { Component, OnInit, ElementRef, Renderer2, ViewChild, Output, EventEmitter, HostListener } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;

@Component({
  selector: 'app-capture-photo',
  templateUrl: './capture-photo.component.html',
  styleUrls: ['./capture-photo.component.scss']
})
export class CapturePhotoComponent implements OnInit {
  videoWidth = 0;
  videoHeight = 0;
  showImage = false;
  liveMsg = false; 
  camstream: any;
  img;
  dataurl;
  socketMsg
  constructor(private renderer: Renderer2, private _fd: FetchDataService, private chat: ChatService) { }
  @ViewChild('video', { static: true }) videoElement: ElementRef;
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  @Output('myOutputVal') myOutputVal = new EventEmitter();

  ngOnInit(): void {

    // this.startCamera();
    $('.videoData').show();
    this.chat.getconnect('toujeo-135');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      let check = data.split('_');
      if(check[0]=="start" && check[1]=="live"){
        this.liveMsg = true;
        console.log(check[2]);
        this.socketMsg = check[2];
      }
      if(check[0] == 'stop' && check[1]== 'live'){
        this.liveMsg = false;
       }

    }));
    let modal = document.getElementById("myModal");
    window.onclick = (event) => {
      if (event.target == modal) {
        this.closeCamera();
      }
    }
  }

  openCapturePhotoModal() {
    $('.capturePhotoModal').modal('show');
    this.startCamera();
  }
  constraints = {
    facingMode: { exact: 'environment' },
    video: {
      width: { ideal: 720 },
      height: { ideal: 480 }
    }
  };

  startCamera() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
    } else {
      alert('Sorry, camera not available.');
    }

  }
  handleError(error) {
    console.log('Error: ', error);
  }
  stream: any;
  attachVideo(stream) {
    this.camstream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', this.camstream);
    this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }
  stopStream() {
    if (null != this.camstream) {

      var track = this.camstream.getTracks()[0];

      track.stop();
      this.videoElement.nativeElement.load();

      this.camstream = null;
    }
  }
  capture() {
    this.showImage = true;
    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    // this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
    this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, this.videoWidth/3,this.videoHeight/3, this.videoWidth/2.5, this.videoHeight/2.5);
    let canvas: any = document.getElementById("canvas");
    let context = canvas.getContext('2d');
    context.strokeStyle = "#ffc90e";
    context.lineWidth = 2;
    let vid: any = document.getElementById("vid");
    // var logo = document.getElementById("logo");
    // context.drawImage(logo, 5, 5, 150, 70);
    // context.strokeRect(0, 0, canvas.width, canvas.height);
    // let imgPath = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/img/xtrem.jpeg';
    // let imgPath = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/img/watermark.png';
    // let imgObj = new Image();
    // imgObj.src = imgPath;
    let watermark = new Image();
    let headerimg = new Image();
    context.beginPath();
    context.lineWidth = 4;
    context.strokeStyle = "#fff";
    context.fillStyle = "#FFFFFF";
    let btmRect = vid.videoHeight - 10;
    // context.fillRect(0, btmRect, vid.videoWidth, 50);
    // context.textAlign = 'left';
    // context.fillStyle = 'black';
    // context.font = '14pt sans-serif';
    // context.textBaselinke = 'middle';
    // let textString = 'Powered To Do More';
    // let textWidth = context.measureText(textString).width;
    // context.fillText(textString, (canvas.width / 2 - 90), (canvas.height - 15), 360);
    context.stroke();
    context.beginPath();
    // watermark.src = 'https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/serdia_myanmar/images/btm.png';
    // context.drawImage(watermark, 0, (btmRect));
    headerimg.src = 'assets/photobooth.png';
  //  context.drawImage(watermark, 0, btmRect, vid.videoWidth, 52);
    context.drawImage(headerimg, 0, 0, vid.videoWidth, vid.videoHeight);
    this.dataurl = canvas.toDataURL('mime');
    // this.img = canvas.toDataURL("image/png");
    this.img = canvas.toDataURL("image/jpeg", 0.7);
    // console.log('kkkk', imgObj.src)
    // $('.videoData').hide();
  }

  uploadVideo() {
    console.log('test');
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    let user_name = JSON.parse(localStorage.getItem('virtual')).name;
    const formData = new FormData();
    formData.append('user_id', user_id);
    formData.append('user_name', user_name);
    formData.append('image', this.img);
    this._fd.uploadsample(formData).subscribe(res => {
      console.log('upload', res)
    });
  }

  showCapture() {
    $('.capturePhoto').modal('show');
    this.startCamera();
  }

  closeCamera() {
    // location.reload();
    this.stopStream();
    $('.capturePhotoModal').modal('hide');

  }
  reload() {
    this.showImage = false;
    this.startCamera();
  }
  @HostListener('keydown', ['$event']) onKeyDown(key) {
    if (key.keyCode === 27) {
      this.closeCamera();
    }
  }
}
