import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token;
  msg;
  //loginForm:FormGroup
  profileForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    pass: new FormControl(''),
  });
  coverImage = "https://d3ep09c8x21fmh.cloudfront.net/https://d3ep09c8x21fmh.cloudfront.net/assets/iffi/iffi/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    //var countDownDate = new Date("Jan 16, 2021 14:00:00").getTime();

    // Update the count down every 1 second
    //var x = setInterval(function() {
    
      // Get today's date and time
     // var now = new Date().getTime();
        
      // Find the distance between now and the count down date
//var distance = countDownDate - now;
      
      // Time calculations for days, hours, minutes and seconds
      // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      // var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      // var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      //  Output the result in an element with id="demo"
    //  document.getElementById("demo").innerHTML =  hours + "h "
      //+ minutes + "m " + seconds + "s ";
     
        
      // If the count down is over, write some text 
      // if (distance < 0) {
      //   clearInterval(x);
      //   document.getElementById("demo").innerHTML = "";
      // document.getElementById("open").style.display = "block";
      // document.getElementById("close").style.display = "none";
    
      // }
     
   // }, 1000);
    //this.times();
  //  currentDate.setDate(currentDate.getDate());
   // alert(currentDate)
    // let my_date:any = new Date('Jan 16, 2021 10:10:10');
    // setInterval(()=>{
    //   let currentDate:any = new Date().getTime();
    //   let setDate = new Date(my_date).getTime();    
    //   let duration:any = setDate - currentDate;
    //   let hh:any = duration/(1000*60*60);
    //   let mm:any = duration/(1000*60);
    //   let ss:any = duration/(1000);

    //   //console.log(hh+mm+ss);
    // }, 1000);

    localStorage.setItem('user_guide', '');
    // this.loginForm = this.formBuilder.group({
    //   email: ['',[Validators.email,
    //    Validators.pattern("[^ @]*@[^ @]*"),
    //       emailDomainValidator]]
    // });
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }

    
  }
  
skipButton() {
    let pauseVideo: any = document.getElementById("myVideo");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    this.videoPlay=false;
    this.router.navigateByUrl('/auditorium/one');
  }
  loggedIn() {
    console.log('logindata', this.profileForm.value);
    
      // const user = {
      //   name : this.profileForm.value.name,
      //   email: this.profileForm.value.email,
      //   pass : this.profileForm.value.pass,
      //  // password: this.loginForm.get('password').value,
      //   event_id: 131,
      //   role_id: 1
      // };
      const formData = new FormData();
      formData.append('event_id', '131');
      formData.append('name', this.profileForm.value.name);
      formData.append('email', this.profileForm.value.email);
     // formData.append('headquarter', this.signupForm.get('job_title').value);
      var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
        
    };
    if( isMobile.iOS() ){
      this.videoPlay = false;
      this.router.navigateByUrl('/auditorium/one');
    }
    this.videoPlay = true;
    let vid: any = document.getElementById('myVideo');
    vid.play();
   
     // console.log("password",user.password)
      // this.auth.loginMethod(this.profileForm.value.email).subscribe((res: any) => {
      //   if (res.code === 1) {
      //     if( isMobile.iOS() ){
      //       this.videoPlay = false;
      //       this.router.navigateByUrl('/lobby');
      //     }
      //     this.videoPlay = true;
      //     localStorage.setItem('virtual', JSON.stringify(res.result));
      //     this.videoPlay = true;
      //     let vid: any = document.getElementById('myVideo');
      //     vid.play();
      //     if (window.innerHeight>window.innerWidth){
      //       this.potrait = true;
      //     }else{
      //       this.potrait = false;
      //     }
      //   } else {
      //     this.msg = 'Invalid Login';
      //     this.videoPlay = false;
      //     this.profileForm.reset();
      //   }
      // }, (err: any) => {
      //   this.videoPlay = false;
      //   console.log('error', err)
      // });
   
  this.profileForm.reset();
    // this._fd.authLogin(user).subscribe(res => {
    //   if (res.code === 1) {
    //     this.videoPlay = true;
    //     localStorage.setItem('virtual', JSON.stringify(res.data));
    //     // this.router.navigate(['/lobby']);
    //     this.videoPlay = true;
    //     let vid: any = document.getElementById('myVideo');
    //     vid.play();
    //   } else {
    //     this.msg = 'Invalid Login';
    //     this.videoPlay = false;
    //     this.loginForm.reset();
    //   }
    // }, err => {
    //   this.videoPlay = false;
    //   console.log('error', err)
    // });
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }
  }
  
  endVideo() {
    // this.videoPlay = false;
    // this.potrait = false;
    this.router.navigateByUrl('/auditorium/one');
    let welcomeAuido:any = document.getElementById('myAudio');
    welcomeAuido.play();
  }
}
