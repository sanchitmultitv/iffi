import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-onvideo',
  templateUrl: './onvideo.component.html',
  styleUrls: ['./onvideo.component.scss']
})
export class OnvideoComponent implements OnInit {
  videoPlays = true;
  constructor(private router:Router) { }

  ngOnInit(): void {
    // let playvideo:any = document.getElementById("myVideo");
    // playvideo.play();
    
  }
  ngAfterViewInit() {
    let playvideo:any = document.getElementById("myVideo");
    playvideo.muted= true;
    playvideo.play();
  }
  gotofirst(){
    
    this.router.navigate(['/firstPage']);
  }
  skipButton() {
    let pauseVideo: any = document.getElementById("myVideo");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    this.videoPlays=false;
    this.router.navigateByUrl('/firstPage');
  }
}
